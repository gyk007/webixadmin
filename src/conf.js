import Root from "Root/root.module"
import Orders from "Orders/orders.module"

export default {
  apiUrl: "https://stage.api.vkusvill.ru/",
  baseUrl: "/admin/orders/",
  modules: {
    root: {
      name: "root",
      hidden: true,
      class: Root
    },
    main: {
      name: "Orders",
      hidden: false,
      icon: "fa-university",
      class: Orders
    }
  }
}
