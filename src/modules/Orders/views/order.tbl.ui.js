import * as webix from "webix"
/**
 * Class POUI
 */
export default class TableUI {
  constructor() {
    this.id = "Orders"
    return this.ui()
  }

  columns () {
    webix.ui.datafilter.countColumn = webix.extend({
      refresh: function (master, node, value) {
        node.firstChild.innerHTML = "Total records: " + master.count();
      }
    }, webix.ui.datafilter.summColumn);

    return [
      { id: "id_order", header: ["Номер", { content: "textFilter" }], sort: "int", width: 160, footer: { content: "countColumn", colspan: 11, } },
      { id: "shopNo", header: ["Магазин", { content: "textFilter" }], sort: "int", width: 160 },
      {
        id: "date_order", header: ["Дата", { content: "textFilter" }], sort: "raw", template: obj => {
          if (obj.date_order.date) {
            return webix.Date.dateToStr("%Y-%m-%d %H:%i")(obj.date_order.date)
          } else {
            return obj.date_order
          }

        }, width: 160,
      },
      { id: "status_name", header: ["Статус", { content: "textFilter" }], sort: "string", width: 160 },
      { id: "sum_order", header: ["Сумма", { content: "textFilter" }], sort: "raw" },
      { id: "taxi_status", header: ["Статус доставки", { content: "textFilter" }], sort: "string", fillspace: true },
      { id: "id_gettaxi", header: ["Get ID", { content: "textFilter" }], sort: "int", width: 160 },
      { id: "id_user", header: ["Сборщик", { content: "textFilter" }], sort: "string", width: 160 },
      { id: "number", header: ["Карта", { content: "textFilter" }], sort: "string", width: 160 },
      { id: "FullName", header: ["ФИО", { content: "textFilter" }], sort: "string", width: 230 },
      { id: "phone", header: ["Телефон", { content: "textFilter" }], sort: "int", width: 160 },
    ]
  }

  table () {
    return {
      id: this.id + 'Table',
      container: "MainContent",
      view: "datatable",
      select: "row",
      footer: true,
      height: document.body.offsetHeight - 120,
      selected: true,
      columns: this.columns()
    }
  }

  toolbar () {
    return {
      view: "toolbar",
      cols: [
        { width: 15 },
        { view: "label", id: this.id + "Lable", label: "Заказы", width: 80 },
        { width: 10 },
        {
          view: "datepicker",
          id: this.id + "DateForm",
          value: null,
          label: "От:",
          labelWidth: 30,
          width: 150,
        },
        { width: 10 },
        {
          view: "datepicker",
          id: this.id + "DateTo",
          value: null,
          label: "До:",
          labelWidth: 30,
          width: 150,
        },
        { width: 10 },
        {
          view: "combo", value: '2', width: 150, id: this.id + 'Status', options: [
            { id: '2', value: "GetTaxy" },
            { id: '0', value: "Express" },
          ],
        },
        {},
        { view: "button", id: this.id + "BtnShowChangeStatus", label: "Изменить статус", width: 150, hidden: true },
        { view: "button", id: this.id + "BtnShowOrderWnd", label: "Детали заказа", width: 150, hidden: true },
        { width: 20 }
      ]
    }
  }

  ui () {
    return {
      id: this.id,
      view: 'layout',
      container: "MainContent",
      rows: [
        this.toolbar(),
        this.table()
      ]
    }
  }
}