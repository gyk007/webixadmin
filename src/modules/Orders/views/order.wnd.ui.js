/**
* Class OrderWindowUI
*/
export default class OrderWindowUI {
  /* Конструктор */
  constructor() {
    this.id = 'OrderWindow';
    this.window_title = 'Детали заказа';
    return this.ui();
  }

  /* Форма в окне*/
  form () {
    return {
      type: "clean",
      rows: [
        {
          id: this.id + 'Form',
          view: "form",
          elementsConfig: {
            labelAlign: 'left',
            inputAlign: 'left',
          },
          elements: [
            {
              margin: 4,
              rows: [
                { template: '<div id="WndOrderContent"></div>' }
              ]
            }
          ]
        }
      ]
    };
  }


  /* Окно */
  ui () {
    return {
      id: this.id,
      view: 'window',
      css: 'order_detail',
      fullscreen: false,
      move: false,
      position: 'center',
      width: 300,
      height: 600,
      modal: true,
      head: {
        view: "toolbar", cols: [
          { view: "button", id: this.id + "BtnClose", label: 'Close', width: 100, align: 'left' },
          { view: "label", id: this.id + "Label", label: this.window_title },
        ]
      },
      body: this.form()
    };
  }
}