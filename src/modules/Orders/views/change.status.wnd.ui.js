/**
* Class OrderSelectStatusUI
*/
export default class OrderSelectStatusUI {
  /* Конструктор */
  constructor() {
    this.id = 'OrderSelectStatus';
    this.window_title = 'Статус заказа';
    return this.ui();
  }

  /* Форма в окне*/
  form () {
    return {
      type: "clean",
      rows: [
        {
          id: this.id + 'Form',
          view: "form",
          elementsConfig: {
            labelAlign: 'left',
            inputAlign: 'left',
          },
          elements: [
            {
              margin: 4,
              rows: [
                {
                  cols: [
                    {
                      view: "combo", value: 1, id: this.id + 'Status', options: [
                        { id: 1, value: "Оформлен" },
                        { id: 2, value: "Сборка" },
                        { id: 3, value: "Собран" },
                        { id: 6, value: "Отменен" },
                        { id: 9, value: "Оплачен" },
                      ],
                    },
                    { width: 15 },
                    { view: "button", id: this.id + "BtnChange", label: 'Изменить', width: 100, align: 'left' },
                  ]
                },
              ]
            }
          ]
        }
      ]
    };
  }


  /* Окно */
  ui () {
    return {
      id: this.id,
      view: 'window',
      css: 'cwc_wbx_form',
      fullscreen: false,
      move: false,
      position: 'center',
      width: 400,
      height: 250,
      modal: true,
      head: {
        view: "toolbar", cols: [
          { view: "button", id: this.id + "BtnClose", label: 'Close', width: 100, align: 'left' },
          { view: "label", id: this.id + "Label", label: this.window_title },
        ]
      },
      body: this.form()
    };
  }
}