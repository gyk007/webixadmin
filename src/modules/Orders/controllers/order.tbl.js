/**
 * controller for CatalogTableUI
 */
import Observable from "OneDeckCore/observ"
import * as webix from "webix"
import TableUI from "Orders/views/order.tbl.ui"

export default class Table extends Observable {
  constructor(initObj) {
    super()

    this.api = initObj.api
    this.ui = new TableUI()
    this.id = this.ui.id
    this.app = webix.ui(this.ui)
    this.formatDate = webix.Date.dateToStr("%Y-%m-%d")

    this.htmlLoad = '<div id="fountainG"> <div id="fountainG_1" class="fountainG"></div>  <div id="fountainG_2" class="fountainG"></div>  <div id="fountainG_3" class="fountainG"></div>  <div id="fountainG_4" class="fountainG"></div>  <div id="fountainG_5" class="fountainG"></div>  <div id="fountainG_6" class="fountainG"></div>  <div id="fountainG_7" class="fountainG"></div>  <div id="fountainG_8" class="fountainG"></div></div>';

    this.selectedRow;



    this.eventHandler()
    this.loadData()
  }

  destroy () {
    this.app.destructor()
  }

  eventHandler () {
    $$(this.id + 'Table').attachEvent("onAfterSelect", (id, e, node) => this.selectRow(id))

    $$(this.id + 'BtnShowOrderWnd').attachEvent("onItemClick", () => this.$emit("onShowOrderWnd", this.selectedRow))
    $$(this.id + 'BtnShowChangeStatus').attachEvent("onItemClick", () => this.$emit("onShowChangeStatusWnd", this.selectedRow))


    $$(this.id + 'DateTo').attachEvent("onChange", () => this.loadData())
    $$(this.id + 'DateForm').attachEvent("onChange", () => this.loadData())
    $$(this.id + 'Status').attachEvent("onChange", () => this.loadData())
  }

  selectRow (id) {
    $$(this.id + 'BtnShowOrderWnd').show()
    $$(this.id + 'BtnShowChangeStatus').show()
    this.selectedRow = $$(this.id + 'Table').getItem(id.row)
  }

  loadData () {
    $$(this.id + 'Table').clearAll();
    $$(this.id + 'Table').showOverlay(this.htmlLoad);
    $$(this.id + 'BtnShowOrderWnd').hide()
    $$(this.id + 'BtnShowChangeStatus').hide()

    const arg = {
      gettype: $$(this.id + 'Status').getValue(),
      odb: $$(this.id + 'DateForm').getValue() ? this.formatDate($$(this.id + 'DateForm').getValue()) : null,
      ode: $$(this.id + 'DateTo').getValue() ? this.formatDate($$(this.id + 'DateTo').getValue()) : null,
    }

    webix.ajax().post(this.api + 'darkStore/admin/ordersMonitor', arg, json => {
      const data = JSON.parse(json)
      $$(this.id + 'Table').hideOverlay();

      $$(this.id + 'Table').parse(data);
    });

  }
}
