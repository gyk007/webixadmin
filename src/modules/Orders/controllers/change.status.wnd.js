/**
 * controller for CatalogTableUI
 */
import Observable from "OneDeckCore/observ"
import * as webix from "webix"
import OrderSelectStatusUI from "Orders/views/change.status.wnd.ui"

export default class OrderSelectStatus extends Observable {
  /* Конструктор */
  constructor(initObj) {
    super()

    this.api = initObj.api

    this.ui = new OrderSelectStatusUI();
    this.id = this.ui.id;
    this.wnd = webix.ui(this.ui);

    this.order;

    this.eventHandler();
  }

  /* Обработчик событий */
  eventHandler () {
    $$(this.id + 'BtnClose').attachEvent("onItemClick", () => this.hide())

    $$(this.id + 'BtnChange').attachEvent("onItemClick", () => this.changeStatus())
  }

  changeStatus () {
    const status = $$(this.id + 'Status').getValue()
    this.hide()
  }

  show (order) {
    this.order = order;

    $$(this.id + 'Label').define("label", "Статус заказа № " + this.order.id_order);
    $$(this.id + 'Label').refresh();

    $$(this.id + 'Status').setValue(this.order.id_status)

    this.wnd.show();
  }

  hide () {
    $$(this.id + 'Form').clear();
    $$(this.id + 'Form').clearValidation();
    this.wnd.hide();
  }
}