/**
 * controller for CatalogTableUI
 */
import Observable from "OneDeckCore/observ"
import * as webix from "webix"
import OrderWindowUI from "Orders/views/order.wnd.ui"

export default class OrderWindow extends Observable {
  /* Конструктор */
  constructor(initObj) {
    super()

    this.api = initObj.api

    this.ui = new OrderWindowUI();
    this.id = this.ui.id;
    this.wnd = webix.ui(this.ui);
    this.order;

    this.eventHandler();
  }

  /* Обработчик событий */
  eventHandler () {
    $$(this.id + 'BtnClose').attachEvent("onItemClick", () => this.hide())
  }

  show (order) {
    this.order = order;

    ['closedate', 'date_order', 'date_status', 'opendate', 'packagetime'].forEach(fildName => this.orderDateFormat(fildName))

    this.wnd.show();

    $$(this.id + 'Label').define("label", "Детали заказа № " + this.order.id_order);
    $$(this.id + 'Label').refresh();

    let content = '';
    Object.keys(this.order).forEach(key => {
      content += `<p class='order_detail'> <b>${key}</b>: ${this.order[key]}; </p>`
    })

    document.getElementById('WndOrderContent').innerHTML = content
  }

  orderDateFormat (fildName) {
    this.order[fildName] = this.order[fildName] && this.order[fildName].date
      ? webix.Date.dateToStr("%Y-%m-%d %H:%i")(this.order[fildName].date)
      : this.order[fildName]
  }


  hide () {
    document.getElementById('WndOrderContent').innerHTML = ''
    this.wnd.hide();
  }
}