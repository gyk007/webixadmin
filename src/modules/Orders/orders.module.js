import "webix/webix.css"
import "webix/skins/mini.min.css"
import "Orders/scss/main.scss"

import OrderTbl from "Orders/controllers/order.tbl"
import OrderWnd from "Orders/controllers/order.wnd"
import ChangeStatusWnd from "Orders/controllers/change.status.wnd"

import Module from "OneDeckCore/module"


/**
 * Class Orders
 * module use Webix
 */
export default class Orders extends Module {
  init (path, state) {

    this.OrderTbl = new OrderTbl({ api: this.$$config.apiUrl })
    this.OrderWnd = new OrderWnd({ api: this.$$config.apiUrl })
    this.ChangeStatusWnd = new ChangeStatusWnd({ api: this.$$config.apiUrl })

    this.eventHandler()

    this.dispatcher(path, state)
  }

  destroy () {
    this.OrderTbl.destroy()
  }

  eventHandler () {
    this.OrderTbl.$on("onShowOrderWnd", order => {
      this.OrderWnd.show(order)
    })

    this.OrderTbl.$on("onShowChangeStatusWnd", order => {
      this.ChangeStatusWnd.show(order)
    })
  }
}
