/**
 * Class Root
 */
import "webix/webix.css"
import "webix/skins/mini.min.css"
import * as webix from "webix"
import RootModule from "OneDeckCore/root.module"

import "Root/scss/main.scss"
import RootContent from "Root/controllers/root.content"
import RootMenu from "Root/controllers/root.menu"

export default class Root extends RootModule {
  init () {
    this.Content = new RootContent()
    this.Menu = new RootMenu(this.$$config)

    this.eventHandler()
  }

  eventHandler () {
    webix.attachEvent("onAjaxError", this.ajaxError)

    this.Content.$on("openMenu", () => {
      this.Menu.show()
    })

    this.Menu.$on("initModule", data =>
      this.$$rout({
        path: data.url,
        state: data.state
      })
    )
  }

  ajaxError () {
    webix.confirm({
      title: "SERVER ERROR",
      ok: "Yes",
      cancel: "No",
      type: "confirm-error",
      text: "Please reload the page",
      callback: result => {
        if (result) {
          document.location.reload(true)
        }
      }
    })
  }
}
